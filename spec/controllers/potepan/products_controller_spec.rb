require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "Get show" do
    let(:taxon) { create(:taxon, name: "Bags") }
    let(:product) { create(:product, name: 'Tote', taxons: [taxon]) }
    let!(:related_product) { create(:product, name: 'Backpack', taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it "response product detail page" do
      expect(response).to have_http_status(:ok)
    end

    it "assign @product" do
      expect(assigns(:product)).to eq product
    end

    it "assign @related_products" do
      expect(assigns(:related_products)).to contain_exactly related_product
    end

    it "renders the :show template" do
      expect(response).to render_template :show
    end
  end
end
