require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let(:taxonomy1) { create(:taxonomy, name: "Category") }
  let(:taxonomy2) { create(:taxonomy, name: "Brand") }
  let(:taxon) { taxonomy1.root.children.create(name: "Bag") }
  let(:product1) { create(:product, name: 'Ruby on Rails Tote', taxons: [taxon]) }
  let(:product2) { create(:product, name: 'Ruby on Rails Bag', taxons: [taxon]) }

  before do
    get :show, params: { id: taxon.id }
  end

  it "response category page" do
    expect(response).to have_http_status(:ok)
  end

  it "assign @taxon" do
    expect(assigns(:taxon)).to eq taxon
  end

  it "assign @products" do
    expect(assigns(:products)).to match_array([product1, product2])
  end

  it "assign @taxonomies" do
    expect(assigns(:taxonomies)).to match_array([taxonomy1, taxonomy2])
  end
end
