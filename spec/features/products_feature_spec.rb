require 'rails_helper'

RSpec.feature 'products', type: :feature do
  feature "show画面" do
    given!(:taxonomy) { create(:taxonomy, name: "Category") }
    given!(:taxon) { taxonomy.root.children.create(name: "Bag") }
    given!(:product) { create(:product, name: 'Ruby on Rails Tote', price: 15.99, description: "foo", taxons: [taxon]) }
    given!(:related_product) { create(:product, name: 'Ruby on Rails Bag', taxons: [taxon]) }
    given!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      visit potepan_product_path(product.id)
    end

    scenario 'Home画面へのリンク確認' do
      expect(page).to have_link 'Home'
      has_link?('Logo')
    end

    scenario '商品名の表示' do
      expect(page).to have_selector 'h2', text: 'Ruby on Rails Tote'
    end

    scenario '価格の表示' do
      expect(page).to have_selector 'h3', text: '$15.99'
    end

    scenario '説明文の表示' do
      expect(page).to have_selector 'p', text: 'foo'
    end

    scenario '"一覧ページへ戻る"のリンク' do
      within '.media-body' do
        expect(page).to have_link '一覧ページへ戻る', href: potepan_category_path(taxon.id)
      end
    end

    scenario '関連商品の詳細ページへのリンク' do
      expect(page).to have_link related_product.name, href: potepan_product_path(related_product.id)
    end

    scenario '関連商品にページの商品が表示されないこと' do
      expect(page).not_to have_selector 'h5', text: product.name
    end

    scenario '関連商品が4つを超えて表示されていないこと' do
      expect(product.related_products(4).count).to eq 4
    end
  end
end
