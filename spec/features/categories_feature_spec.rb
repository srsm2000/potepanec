require 'rails_helper'

RSpec.feature 'Categories', type: :feature do
  feature "カテゴリーページの表示とリンク" do
    given!(:taxonomy) { create(:taxonomy, name: 'Category') }
    given!(:taxon1) { taxonomy.root.children.create(name: 'Bags') }
    given!(:taxon2) { taxonomy.root.children.create(name: 'Shirts') }
    given!(:product1) { create(:product, name: 'Tote', taxons: [taxon1]) }
    given!(:product2) { create(:product, name: 'Shirts A', taxons: [taxon2]) }

    before do
      visit potepan_category_path(taxon1.id)
    end

    scenario 'Home画面へのリンク' do
      expect(page).to have_link 'Home'
      has_link?('Logo')
    end

    scenario '商品名の表示とリンク' do
      expect(page).to have_selector 'h5', text: product1.name
      expect(page).to have_link product1.name, href: potepan_product_path(product1.id)
    end

    scenario '商品の価格の表示' do
      expect(page).to have_selector 'h3', text: product1.display_price
    end

    scenario "他カテゴリーページの正常な作動" do
      expect(page).not_to have_selector 'h5', text: product2.name
      within '.collapseItem' do
        click_link taxon2.name
      end
      expect(page).to have_title("Shirts - BIGBAG Store")
      expect(page).not_to have_selector 'h5', text: product1.name
      expect(page).to have_selector 'h5', text: product2.name
    end
  end
end
