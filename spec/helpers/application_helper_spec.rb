require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "title表示" do
    let(:product) { create(:product, name: 'T-shirts') }

    it "product.nameが存在するときのtitle" do
      expect(full_title(product.name)).to eq("T-shirts - BIGBAG Store")
    end

    it "product.nameが空のときのtitle" do
      expect(full_title("")).to eq("BIGBAG Store")
    end

    it "product.nameがnilのときのtitle" do
      expect(full_title(nil)).to eq("BIGBAG Store")
    end
  end
end
