module Spree::ProductDecorator
  def related_products(max_number)
    Spree::Product.in_taxons(taxons).includes(master: [:images, :default_price]).where.not(id: id).distinct.sample(max_number)
  end

  Spree::Product.prepend self
end
