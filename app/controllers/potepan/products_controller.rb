class Potepan::ProductsController < ApplicationController
  MAXIMUM_NUMBER_OF_RELATED_PRODUCTS = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products(MAXIMUM_NUMBER_OF_RELATED_PRODUCTS)
  end
end
